Package: autonomics
Title: Automated analysis of omics data
Version: 1.1.2
Authors@R: c(person("Aditya", "Bhagwat", email = "bhagwataditya@gmail.com", role = c("aut", "cre")), 
             person("Johannes", "Graumann", email = 'jog2030@gmail.com', role = 'ctb'), 
             person("Richard", "Cotton", email = "richierocks@gmail.com", role = "ctb"), 
             person("Karsten", "Suhre",  email = "kas2049@qatar-med.cornell.edu", role = "ctb"))
Description: This package offers an automated interface for the statistical analysis of omics data.
Depends:
    R (>= 3.4.0)
License: GPL-3
LazyData: true
Imports:
    assertive.properties,
    assertive.types,
    autonomics.annotate,
    autonomics.explore,
    autonomics.find,
    autonomics.import,
    autonomics.integrate,
    autonomics.ora,
    autonomics.plot,
    autonomics.preprocess,
    autonomics.support,
    ggplot2,
    magrittr,
    plyr,
    stringr,
    utils
RoxygenNote: 6.0.1
Suggests: 
    autonomics.data,
    alnoubi.2017,
    atkin.2014,
    billing.differentiation.data,
    halama.2016,
    knitr,
    stringi,
    statmod,
    subramanian.2016,
    testthat
VignetteBuilder: knitr
